#ifndef HANDDETECTION_LEAP_HAND_PROCESS_HPP_
#define HANDDETECTION_LEAP_HAND_PROCESS_HPP_

#include <Leap.h>
#include <LeapMath.h>

class Rotation3D;
struct Quaternion
{
  double x, y, z, w;
  Quaternion() : x(0.0), y(0.0), z(0.0), w(0.0)
  {
  }
  Quaternion(double x, double y, double z, double w) : x(x), y(y), z(z), w(w)
  {
  }
  Quaternion(const Rotation3D&);
};

struct Rotation3D
{
  double R_[3][3];
  const double* operator[](int index) const
  {
    return index < 3 ? R_[index] : NULL;
  }
  double* operator[](int index)
  {
    return index < 3 ? R_[index] : NULL;
  }
  double& operator()(unsigned int index, unsigned int index2)
  {
    index = index % 3;
    index2 = index2 % 3;
    return R_[index][index2];
  }
};

class LeapHandProcess
{
 public:
  LeapHandProcess();
  Leap::Vector processHandPosition(const Leap::Hand& hand);
  Quaternion processHandRotation(const Leap::Hand& hand);
  double processGrab(const Leap::Hand& hand);
  double processSphereGrab(const Leap::Hand& hand);

  void setStartFrame(const Leap::Frame&);

 private:
  Leap::Frame start_frame_;
};

#endif