\mainpage
<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-generate-toc again -->
**Table of Contents**

- [caros_kuka_robot](#caroskukarobot)
- [Interfaces - how to use this node](#interfaces---how-to-use-this-node)
- [Requirements](#requirements)
- [Launching the node](#launching-the-node)
  - [Parameters](#parameters)
- [Small demo(s)](#small-demos)

<!-- markdown-toc end -->

# caros_kuka_robot #
caros_kuka_robot is a ROS node for controlling a physical Kuka Robot. A few interfaces are available for controlling the robot.

# Interfaces - how to use this node #


# Requirements #
Robwork is required and can be obtained from http://www.robwork.dk

# Launching the node #
The CAROS Kuka node can be launched by using the following:

    roslaunch caros_kuka_lwr caros_kuka_robot.launch

## Parameters ##
The following parameters are supported:
| Parameter | Description | Default |
| --------- | ----------- | ------- |
| device_ip | IP of the kuka robot | 172.31.1.14 |
| device_port | The device port | 49152 |
| ft_topic | Where to publish force torque info | kuka_ft |

# Small demo(s) #
To quickly and easily verify that the component start up the component using the launch script (see above) and use rqt to view the topic *wrench* and the *caros_node_state*.
