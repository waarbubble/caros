#ifndef CAROS_BASLER_CAMERA_H
#define CAROS_BASLER_CAMERA_H

#include <caros/camera_interface.h>
#include <caros_camera/BaslerConfig.h>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>

// GigE (Pylon)
#include <pylon/PylonIncludes.h>
#include <pylon/gige/BaslerGigEInstantCamera.h>
#include <pylon/gige/BaslerGigECamera.h>
#include <pylon/gige/_BaslerGigECameraParams.h>

#include <string>
#include <map>

namespace caros
{
class BaslerCamera : public CameraInterface
{
 public:
  // constructors
  BaslerCamera();
  explicit BaslerCamera(const std::string& serial);
  ~BaslerCamera() override;

  GenApi::INodeMap& setup(Pylon::CInstantCamera& camera);

  bool setNewConfiguration(caros_camera::BaslerConfig& config, const uint32_t& level) override;

  std::string getName() const override;

  std::string getSerial() const override;

  bool getRawImage(sensor_msgs::ImagePtr& img, uint64_t& timestamp) override;

  bool init() override;

  void start() override;

  void stop() override;

  bool shutdown() override;

  bool isRunning() const override;

 protected:
  Pylon::CBaslerGigEInstantCamera gige_camera_;
  bool gige_camera_initialized_{};
  std::string cam_encoding_;

  Pylon::DeviceInfoList_t gige_cam_list_;
  int nr_of_gige_cameras_{};
};
}  // namespace caros

#endif  // CAROS_BASLER_CAMERA_H
