#ifndef CAROS_CAMERA_MANAGER_H
#define CAROS_CAMERA_MANAGER_H

#include <caros/camera_interface.h>
#include <vector>
#include <string>

namespace caros
{
class CameraManager
{
 public:
  CameraManager();
  ~CameraManager();

  enum CAMERA_TYPE
  {
    BASLER = 0,
    BUMBLEBEE,
    ENSENSO
  };

  int getCameraType();

  unsigned int getSerial();

  std::shared_ptr<CameraInterface> getCamera(unsigned int serial, int camera_type);

  std::shared_ptr<CameraInterface> getCamera(std::string name, int camera_type);

 protected:
  int camera_type_;
  unsigned int serial_;
};
}  // namespace caros

#endif  // CAROS_CAMERA_MANAGER_H
